module.exports = {
    ident: "postcss",
    plugins: {
      'postcss-import': { path: '/' },
      'postcss-nesting': {},
      'postcss-css-variables': {
          preserveInjectedVariables: false
      },
      'autoprefixer': {
        grid: 'autoplace'
      }
    }
  }
  