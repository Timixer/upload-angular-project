module.exports = {
    module: {
      rules: [
        {
          test: /\.scss$/,
          enforce: "pre",
          exclude: /node_modules/,
          use: [
            'postcss-loader'
          ]
        }
      ]
    }
  }
  