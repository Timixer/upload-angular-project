# Angular Project

## Design Files
You can find the associated files that are needed for building this project on [Figma](https://www.figma.com/file/1rdMzmbuLCj5sn4D1ehRu7/Angular-Project?node-id=0%3A1). You can create a free account to access the developer tools. This is the tool we use daily.

You only need to build the Anxiety and Depression pages (as provided in Figma). The code should be responsive and there should be a way jump between Anxiety and Depression while including a smooth animation. 

This project should be AODA compliant. There have been some styles provided to be used as guidance as an example of how we currently starting to work but you're free to build however you'd like.

There's a JSON file which is already being imported - you'll just need to display the data. 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
