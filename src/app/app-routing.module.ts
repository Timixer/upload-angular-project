import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgramsComponent } from './components/programs/programs.component';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    { path: '', component: ProgramsComponent, data: { title: 'Programs' } }
  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
