import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ProgramsComponent } from './components/programs/programs.component';
import { LogoComponent } from './components/header/logo/logo.component';
import { NavComponent } from './components/header/nav/nav.component';
import { SecondaryNavComponent } from './components/programs/secondary-nav/secondary-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProgramsComponent,
    LogoComponent,
    NavComponent,
    SecondaryNavComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }