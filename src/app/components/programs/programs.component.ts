import { Component, NgModule, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import programsList from '../../../assets/json/programLists.json';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.scss']
})

export class ProgramsComponent implements OnInit {
  lang: string;
  public programList = programsList.programs;
  programLists: string [];

  constructor (private httpService: HttpClient) { }

  ngOnInit() {
    this.httpService.get('../../../assets/json/programLists.json').subscribe(
      data => {this.programLists = data as string [];
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }

}
